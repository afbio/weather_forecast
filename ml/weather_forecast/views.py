from django.core.exceptions import ValidationError
from django.forms import fields
from django.http import JsonResponse
from django.views import View

from django.http import (
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseNotFound,
    HttpResponseServerError,
)

from ml.weather_forecast.domain.consts import YEARS_QTY, COMPLETE_ORBIT_DAYS
from ml.weather_forecast.jobs.calculate_forecast_job import CalculateForecastJob
from ml.weather_forecast.models import Forecast


class WeatherForecastView(View):

    # Considering the initial day = 0, then 3600 - 1
    MAX_DAY_FORECAST = (YEARS_QTY * COMPLETE_ORBIT_DAYS) - 1

    def get(self, request):
        try:
            day = fields.IntegerField(min_value=0, max_value=self.MAX_DAY_FORECAST).clean(
                request.GET.get('day')
            )

            forecast = Forecast.objects.get(day=day)
            return JsonResponse({'dia': day, 'clima': forecast.weather})
        except ValidationError:
            return JsonResponse(
                {'error': f'You should inform a valid day between 0 and {self.MAX_DAY_FORECAST}'},
                status=HttpResponseBadRequest.status_code
            )
        except Forecast.DoesNotExist:
            return JsonResponse(
                {'error': 'Not found forecast for this day'},
                status=HttpResponseNotFound.status_code
            )


class WeatherForecastAppEngineCronView(View):

    def get(self, request):
        is_called_by_app_engine = request.headers.get('X-Appengine-Cron', False)

        if not is_called_by_app_engine:
            return JsonResponse(
                {'error': 'Forbidden request'},
                status=HttpResponseForbidden.status_code
            )

        success, result = CalculateForecastJob.run()
        return JsonResponse(
            {'success': success, 'result': result},
            status=HttpResponseServerError.status_code if not success else None,
            json_dumps_params={'ensure_ascii': False}
        )
