"""
Considering the needed days for the slowest planet complete an orbit around the Sun
"""
COMPLETE_ORBIT_DAYS = 360

"""
Years quantity for being calculated the forecast
"""
YEARS_QTY = 10