from enum import Enum


class RotationDirection(Enum):
    CLOCKWISE = 1
    COUNTERCLOCKWISE = 2


class Weather(Enum):
    DROUGHT = 'sequia'
    RAIN = 'lluvia'
    GREAT_CONDITIONS = 'óptimas condiciones de presión y temperatura'
    UNKNOWN = 'desconocido'