from typing import List

from ml.weather_forecast.domain.enums import Weather
from ml.weather_forecast.domain.planet import Planet
from ml.weather_forecast.geometry.line import Line
from ml.weather_forecast.geometry.point import Point
from ml.weather_forecast.geometry.triangle import Triangle


class Galaxy:

    SUN_POINT = Point(0, 0)

    def __init__(self, planets: List[Planet]) -> None:
        if len(planets) != 3:
            raise ValueError("You must set 3 planets")

        self.planets = planets

    def get_weather_forecast_for_day(self, day: int) -> Weather:
        if self.__is_aligned_planets(day):

            if self.__is_aligned_planets_with_sun(day):
                return Weather.DROUGHT

            return Weather.GREAT_CONDITIONS

        if self.__is_sun_inside_triangle(day):
            return Weather.RAIN

        return Weather.UNKNOWN

    def get_triangle_perimeter(self, day):
        return self.__get_planets_triangle(day).get_perimeter()

    def __get_planets_triangle(self, day: int) -> Triangle:
        return Triangle(
            self.planets[0].get_point_for_day(day),
            self.planets[1].get_point_for_day(day),
            self.planets[2].get_point_for_day(day)
        )

    def __is_aligned_planets(self, day: int) -> bool:
        line = Line(self.planets[0].get_point_for_day(day), self.planets[1].get_point_for_day(day))
        return line.contains_point(self.planets[2].get_point_for_day(day))

    def __is_aligned_planets_with_sun(self, day: int) -> bool:
        line = Line(self.planets[0].get_point_for_day(day), self.planets[1].get_point_for_day(day))
        return line.contains_point(self.SUN_POINT)

    def __is_sun_inside_triangle(self, day: int) -> bool:
        triangle = self.__get_planets_triangle(day)
        return triangle.contains_point(self.SUN_POINT)
