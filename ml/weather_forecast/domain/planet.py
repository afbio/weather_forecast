import math

from ml.weather_forecast.domain.consts import COMPLETE_ORBIT_DAYS
from ml.weather_forecast.domain.enums import RotationDirection
from ml.weather_forecast.geometry.point import Point


class Planet:

    def __init__(self,
                 name: str,
                 distance_from_sun: int,
                 degrees_by_day: int,
                 direction: RotationDirection = RotationDirection.CLOCKWISE
                 ) -> None:
        self.name = name
        self.distance_from_sun = distance_from_sun
        self.degrees_by_day = degrees_by_day
        self.direction = direction

    def get_point_for_day(self, day: int) -> Point:
        current_angle = (self.degrees_by_day * day) % COMPLETE_ORBIT_DAYS

        if self.direction == RotationDirection.CLOCKWISE and current_angle > 0:
            current_angle = COMPLETE_ORBIT_DAYS - current_angle

        # Polar -> Cartesian conversion
        current_angle_in_radians = math.radians(current_angle)
        coord_x = self.distance_from_sun * math.cos(current_angle_in_radians)
        coord_y = self.distance_from_sun * math.sin(current_angle_in_radians)

        return Point(round(coord_x, 2), round(coord_y, 2))
