from django.db import models

class Forecast(models.Model):
    day = models.IntegerField(unique=True)
    weather = models.CharField(max_length=50)
