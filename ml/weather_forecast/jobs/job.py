from typing import Tuple, Dict


class Job:
    """
    :returns A tuple formed by (boolean flag success, dict with specific data)
    """
    def run(self) -> Tuple[bool, Dict]:
        raise NotImplemented
