from typing import Tuple, Dict

from django.db.transaction import atomic

from ml.weather_forecast.domain.consts import COMPLETE_ORBIT_DAYS, YEARS_QTY
from ml.weather_forecast.domain.enums import RotationDirection, Weather
from ml.weather_forecast.domain.galaxy import Galaxy
from ml.weather_forecast.domain.planet import Planet
from ml.weather_forecast.jobs.job import Job
from ml.weather_forecast.models import Forecast


class CalculateForecastJob(Job):
    """
    This job is responsible for the forecast calculation for the next YEARS_QTY years.
    """

    @classmethod
    @atomic
    def run(cls) -> Tuple[bool, Dict]:
        """
        Executes atomic clean and fill operations on the Forecast table.
        Also calculates the number of periods and days for each weather type and finds the day
        with the max rain intensity.
        :return: A tuple (boolean flag success, dict with data about periods qty + max rain day)
        """
        planets = [
            Planet("Ferengi", 500, 1, RotationDirection.CLOCKWISE),
            Planet("Betasoide", 2000, 3, RotationDirection.CLOCKWISE),
            Planet("Vulcano", 1000, 5, RotationDirection.COUNTERCLOCKWISE)
        ]

        galaxy = Galaxy(planets)

        weather_periods_count = cls.__get_counter_for_weather()
        weather_days_count = cls.__get_counter_for_weather()

        current_weather = None
        max_intensity_in_range = (None, None)

        # Cleaning the forecast table
        Forecast.objects.all().delete()
        forecast_obj_list = []

        try:
            for day in range(0, YEARS_QTY * COMPLETE_ORBIT_DAYS):
                forecast = galaxy.get_weather_forecast_for_day(day)
                forecast_obj_list.append(Forecast(day=day, weather=forecast.value))

                weather_days_count[forecast.value] += 1
                if forecast != current_weather:
                    current_weather = forecast
                    weather_periods_count[forecast.value] += 1

                if forecast == Weather.RAIN:
                    rain_intensity = galaxy.get_triangle_perimeter(day)
                    if max_intensity_in_range[0] is None \
                            or rain_intensity > max_intensity_in_range[0]:
                        max_intensity_in_range = (rain_intensity, day)


            # Persisting in batch the calculated forecasts
            Forecast.objects.bulk_create(forecast_obj_list)

            result = {
                "periods_count": weather_periods_count,
                "days_count": weather_days_count,
                "max_rain_intensity_day": max_intensity_in_range[1]
            }

            return True, result
        except Exception as e:
            return False, {'exception': str(e)}


    @staticmethod
    def __get_counter_for_weather():
        return {
            Weather.DROUGHT.value: 0,
            Weather.RAIN.value: 0,
            Weather.GREAT_CONDITIONS.value: 0,
            Weather.UNKNOWN.value: 0,
        }
