from django.test import TestCase

from ml.weather_forecast.domain.enums import RotationDirection
from ml.weather_forecast.domain.planet import Planet


class PlanetTestCase(TestCase):

    def test_planet_1(self):
        planet = Planet("Ptest", 500, 1, RotationDirection.CLOCKWISE)
        day_point_map = {
            0: planet.get_point_for_day(0),
            90: planet.get_point_for_day(90),
            180: planet.get_point_for_day(180),
            270: planet.get_point_for_day(270),
        }

        self.assert_((day_point_map[0].x, day_point_map[0].y), (500.0, 0.0))
        self.assert_((day_point_map[90].x, day_point_map[90].y), (0.0, -500.0))
        self.assert_((day_point_map[180].x, day_point_map[180].y), (-500.0, 0.0))
        self.assert_((day_point_map[270].x, day_point_map[270].y), (0.0, 500.0))


    def test_planet_2(self):
        planet = Planet("Ptest2", 2000, 3, RotationDirection.CLOCKWISE)
        day_point_map = {
            0: planet.get_point_for_day(0),
            90: planet.get_point_for_day(90),
            120: planet.get_point_for_day(120),
            150: planet.get_point_for_day(150),
            180: planet.get_point_for_day(180),
        }

        self.assertEqual((day_point_map[0].x, day_point_map[0].y), (2000.0, 0.0))
        self.assertEqual((day_point_map[90].x, day_point_map[90].y), (0.0, 2000.0))
        self.assertEqual((day_point_map[120].x, day_point_map[120].y), (2000.0, 0.0))
        self.assertEqual((day_point_map[150].x, day_point_map[150].y), (0.0, -2000.0))
        self.assertEqual((day_point_map[180].x, day_point_map[180].y), (-2000.0, 0))


    def test_planet_3(self):
        planet = Planet("Ptest3", 1000, 5, RotationDirection.COUNTERCLOCKWISE)
        day_point_map = {
            0: planet.get_point_for_day(0),
            18: planet.get_point_for_day(18),
            36: planet.get_point_for_day(36),
            54: planet.get_point_for_day(54),
        }

        self.assertEqual((day_point_map[0].x, day_point_map[0].y), (1000.0, 0.0))
        self.assertEqual((day_point_map[18].x, day_point_map[18].y), (0.0, 1000.0))
        self.assertEqual((day_point_map[36].x, day_point_map[36].y), (-1000.0, 0.0))
        self.assertEqual((day_point_map[54].x, day_point_map[54].y), (0.0, -1000.0))
