from django.test import TestCase

from ml.weather_forecast.domain.enums import RotationDirection, Weather
from ml.weather_forecast.domain.galaxy import Galaxy
from ml.weather_forecast.domain.planet import Planet


class PlanetTestCase(TestCase):

    galaxy = None

    def setUp(self):
        planets = [
            Planet("Ferengi", 500, 1, RotationDirection.CLOCKWISE),
            Planet("Betasoide", 2000, 3, RotationDirection.CLOCKWISE),
            Planet("Vulcano", 1000, 5, RotationDirection.COUNTERCLOCKWISE)
        ]

        self.galaxy = Galaxy(planets)

    def test_galaxy_drought(self):
        days = [0, 90, 180, 270, 720, 1080, 1620, 2340]

        for day in days:
            forecast = self.galaxy.get_weather_forecast_for_day(day=day)
            self.assertEquals(forecast, Weather.DROUGHT)

    def test_galaxy_rain(self):
        days = [23, 68, 151, 157, 1192, 2189, 2948]

        for day in days:
            forecast = self.galaxy.get_weather_forecast_for_day(day=day)
            self.assertEquals(forecast, Weather.RAIN)

    def test_galaxy_great(self):
        days = [47, 64, 116, 133, 244, 424, 767, 1667]

        for day in days:
            forecast = self.galaxy.get_weather_forecast_for_day(day=day)
            self.assertEquals(forecast, Weather.GREAT_CONDITIONS)
