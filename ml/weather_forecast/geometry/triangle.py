from ml.weather_forecast.geometry.line import Line
from ml.weather_forecast.geometry.point import Point


class Triangle:
    """
    Representing a triangle in a cartesian plane formed by three points
    """

    def __init__(self, point_a: Point, point_b: Point, point_c: Point) -> None:
        self.point_a = point_a
        self.point_b = point_b
        self.point_c = point_c

    def contains_point(self, point: Point) -> bool:
        p = point
        p0 = self.point_a
        p1 = self.point_b
        p2 = self.point_c

        s = p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y
        t = p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y

        if (s < 0) != (t < 0):
            return False

        a = -p1.y * p2.x + p0.y * (p2.x - p1.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y

        return a < 0 if (s <= 0 and s + t >= a) else (s >= 0 and s + t <= a)

    def get_perimeter(self):
        line_0 = Line(self.point_a, self.point_b)
        line_1 = Line(self.point_b, self.point_c)
        line_2 = Line(self.point_c, self.point_a)

        return line_0.get_size() + line_1.get_size() + line_2.get_size()

    def __str__(self):
        return f"({self.point_a}, {self.point_b}, {self.point_c})"
