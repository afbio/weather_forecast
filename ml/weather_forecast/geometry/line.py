import math

from ml.weather_forecast.geometry.point import Point


class Line:
    """
    Representing a line formed by two points
    """
    ALIGNMENT_THRESHOLD = 30


    def __init__(self, point_a: Point, point_b: Point) -> None:
        self.point_a = point_a
        self.point_b = point_b

    def contains_point(self, point: Point) -> bool:
        delta_x = self.point_b.x - self.point_a.x
        delta_y = self.point_b.y - self.point_a.y

        if delta_x == 0:
            return abs(self.point_a.x - point.x) < self.ALIGNMENT_THRESHOLD

        m = delta_y / delta_x

        ey = m * point.x + (self.point_a.y - m * self.point_a.x)
        return abs(ey - point.y) < self.ALIGNMENT_THRESHOLD

    def get_size(self) -> float:
        x_delta = (self.point_b.x - self.point_a.x) ** 2
        y_delta = (self.point_b.y - self.point_a.y) ** 2

        return math.sqrt(x_delta + y_delta)

    def __str__(self):
        return f"(({self.point_a.x}, {self.point_a.y}), ({self.point_b.x}, {self.point_b.y}))"
