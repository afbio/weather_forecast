"""ml URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from ml.weather_forecast.views import WeatherForecastView, WeatherForecastAppEngineCronView

urlpatterns = [
    path('clima', WeatherForecastView.as_view(), name="clima_api"),
    path('app_engine_cron_job', WeatherForecastAppEngineCronView.as_view(), name="clima_job"),
]
